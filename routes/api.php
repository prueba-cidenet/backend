<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(\App\Http\Controllers\EmpleadoController::class)
    ->prefix('empleados')
    ->group(function () {
        Route::get('', 'index');
        Route::get('/{empleado}', 'show');
        Route::post('', 'store');
        Route::put('/{empleado}', 'update');
        Route::delete('/{empleado}', 'destroy');
    });

Route::controller(\App\Http\Controllers\GeneralController::class)
    ->group(function () {
        Route::get('/paises', 'getPaises');
        Route::get('/tiposIdentificacion', 'getTiposIdentificacion');
        Route::get('/areas', 'getAreas');
    });