<?php

namespace App\Enums;

use App\Traits\BasicEnum;

enum Estado: int
{
    use BasicEnum;

    case Inactivo = 0;
    case Activo = 1;
}