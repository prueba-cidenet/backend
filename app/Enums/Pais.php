<?php

namespace App\Enums;

use App\Traits\BasicEnum;

enum Pais: int
{
    use BasicEnum;

    case Colombia = 1;
    case EstadosUnidos = 2;

    public function dominio(): string
    {
        return match($this)
        {
            Pais::Colombia => 'co',
            Pais::EstadosUnidos => 'us'
        };
    }

    public function descripcion(): string
    {
        return match($this)
        {
            Pais::Colombia => 'Colombia',
            Pais::EstadosUnidos => 'Estados Unidos'
        };
    }
}