<?php

namespace App\Enums;

use App\Traits\BasicEnum;

enum TipoIdentificacion: int
{
    use BasicEnum;

    case CedulaCiudadania = 1;
    case CédulaExtranjería = 2;
    case Pasaporte = 3;
    case PermisoEspecial = 4;

    public function descripcion(): string
    {
        return match($this)
        {
            TipoIdentificacion::CedulaCiudadania => 'Cedula de ciudadanía',
            TipoIdentificacion::CédulaExtranjería => 'Cédula de extranjería',
            TipoIdentificacion::Pasaporte => 'Pasaporte',
            TipoIdentificacion::PermisoEspecial => 'Permiso especial',
        };
    }
}