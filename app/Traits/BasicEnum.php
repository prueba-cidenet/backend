<?php

namespace App\Traits;

trait BasicEnum {

    public static function toArray(): array
    {
        $result = [];
        foreach(self::cases() as $pais) {
            $result[$pais->value] = $pais->name;
        }
        return $result;
    }

    public static function columnns(): array
    {
        return array_column(self::cases(), 'value');
    }
}