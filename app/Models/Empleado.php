<?php

namespace App\Models;

use App\Enums\{Pais, TipoIdentificacion};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;

    protected $fillable = [
        'primer_nombre',
        'otros_nombre',
        'primer_apellido',
        'segundo_apellido',
        'pais',
        'tipo_identificacion',
        'identificacion',
        'fecha_ingreso',
        'area_id',
        'fecha_registro',
    ];

    protected $appends = ['pais_nombre', 'tipo_identificacion_nombre'];

    public function getPaisNombreAttribute()
    {
        return Pais::from($this->pais)->descripcion();
    }

    public function getTipoIdentificacionNombreAttribute()
    {
        return TipoIdentificacion::from($this->pais)->descripcion();
    }

    /**
     * Get the area.
     */
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function generarCorreo(): string {
        $correo = "{$this->primer_nombre}.{$this->primer_apellido}@cidenet.com.".Pais::from($this->pais)->dominio();

        $correoRepetido = Empleado::where('correo_electronico', $correo)
            ->count();

        if($correoRepetido > 0) {
            $correo = str_replace('@', "{$correoRepetido}@", $correo);
        }

        return $correo;
    }
}
