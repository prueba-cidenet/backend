<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmpleadoRequest;
use App\Models\Empleado;
use App\Traits\ResponseApi;
use Illuminate\Http\{Request, Response};
use App\Enums\{Estado, Pais, TipoIdentificacion};

class EmpleadoController extends Controller
{
    use ResponseApi;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filtros = json_decode($request->filtros);

        $empleados = Empleado::select(
                'id',
                'pais',
                'area_id',
                'primer_nombre',
                'otros_nombre',
                'primer_apellido',
                'segundo_apellido',
                'tipo_identificacion',
                'identificacion',
                'correo_electronico',
                'fecha_ingreso',
                'estado',
                'fecha_registro',
            )
            ->when($filtros->primer_nombre ?? '', fn($query, $filtro) =>
                $query->where('primer_nombre', 'LIKE', "%$filtro%")
            )
            ->when($filtros->otros_nombre ?? '', fn($query, $filtro) =>
                $query->where('otros_nombre', 'LIKE', "%$filtro%")
            )
            ->when($filtros->primer_apellido ?? '', fn($query, $filtro) =>
                $query->where('primer_apellido', 'LIKE', "%$filtro%")
            )
            ->when($filtros->segundo_apellido ?? '', fn($query, $filtro) =>
                $query->where('segundo_apellido', 'LIKE', "%$filtro%")
            )
            ->when($filtros->tipo_identificacion ?? '', fn($query, $filtro) =>
                $query->where('tipo_identificacion', 'LIKE', "%$filtro%")
            )
            ->when($filtros->identificacion ?? '', fn($query, $filtro) =>
                $query->where('identificacion', $filtro)
            )
            ->when($filtros->pais ?? '', fn($query, $filtro) =>
                $query->where('pais', $filtro)
            )
            ->when($filtros->correo_electronico ?? '', fn($query, $filtro) =>
                $query->where('correo_electronico', 'LIKE', "%$filtro%")
            )
            ->orderBy('id', 'desc')
            ->with('area:id,nombre');

        # Se hace la comprobacion por aparte ya que el '0' no pasa en el when
        if(isset($filtros->estado) && $filtros->estado != '') {
            $empleados->where('estado', $filtros->estado);
        }

        return $this->successResponse($empleados->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpleadoRequest $request)
    {
        $empleado = new Empleado;
        $empleado->fill($request->all());
        $empleado->correo_electronico = $empleado->generarCorreo();
        $empleado->fecha_ingreso = date('Y-m-d',strtotime($empleado->fecha_ingreso));
        $empleado->fecha_registro = date('Y-m-d H:i',strtotime($empleado->fecha_registro));
        $empleado->save();

        return $this->successResponse($empleado, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        return $this->successResponse($empleado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(EmpleadoRequest $request, Empleado $empleado)
    {
        $empleado->fill($request->except('fecha_registro'));

        if($empleado->isDirty('primer_nombre', 'primer_apellido', 'pais')) {
            $empleado->correo_electronico = $empleado->generarCorreo();
        }

        $empleado->fecha_ingreso = date('Y-m-d',strtotime($empleado->fecha_ingreso));
        $empleado->save();

        return $this->successResponse($empleado);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
        $empleado->estado = (string)Estado::Inactivo->value;
        $empleado->save();

        return $this->successResponse($empleado);
    }
}
