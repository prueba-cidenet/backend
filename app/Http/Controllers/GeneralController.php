<?php

namespace App\Http\Controllers;

use App\Traits\ResponseApi;
use App\Enums\{Pais, TipoIdentificacion};
use App\Models\Area;

class GeneralController extends Controller
{
    use ResponseApi;

    /**
     * Display a listing of countries
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaises()
    {
        $paises = array_map(fn(Pais $pais) => [
            'id' => $pais->value,
            'nombre' => $pais->descripcion()
        ], Pais::cases());

        return $this->successResponse($paises);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTiposIdentificacion()
    {
        $tipos = array_map(fn(TipoIdentificacion $tipo) => [
            'id' => $tipo->value,
            'nombre' => $tipo->descripcion()
        ], TipoIdentificacion::cases());

        return $this->successResponse($tipos);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAreas()
    {
        $areas = Area::select('id', 'nombre')
            ->get();

        return $this->successResponse($areas);
    }
}
