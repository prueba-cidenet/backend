<?php

namespace App\Http\Requests;

use App\Enums\{Pais, TipoIdentificacion};
use App\Models\Empleado;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(Request $request)
    {
        $rules = [
            'primer_nombre'         => ['required', 'regex:/^[A-Z]+$/', 'max:20'],
            'otros_nombre'          => ['regex:/^[A-Z]+( [A-Z]+)*$/', 'max:50'],
            'primer_apellido'       => ['required', 'regex:/^[A-Z]+$/', 'max:20'],
            'segundo_apellido'      => ['required', 'regex:/^[A-Z]+$/', 'max:20'],
            'pais'                  => ['required', Rule::in(Pais::columnns())],
            'tipo_identificacion'   => ['required', Rule::in(TipoIdentificacion::columnns())],
            'identificacion'        => [
                'required',
                'regex:/^[0-9a-zñ-]+$/i',
                'max:20',
                Rule::unique('empleados')->where(function ($query) use($request) {
                    return $query->where('tipo_identificacion', $request->tipo_identificacion)
                        ->where('identificacion', $request->identificacion);
                }),
            ],
            'fecha_ingreso'         => 'required|date|after:-1 month|before:tomorrow',
            'area_id'               => 'required|exists:areas,id',
            'fecha_registro'        => 'required|date',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $empleado = request()->route('empleado');

            $rules['identificacion'] = [
                'required',
                'regex:/^[0-9a-zñ-]+$/i',
                'max:20',
                Rule::unique('empleados')->where(function ($query) use($request, $empleado) {
                    return $query->where('tipo_identificacion', $request->tipo_identificacion)
                        ->where('identificacion', $request->identificacion)
                        # Ignore en la validacion el usuario actual
                        ->whereNot(function($query) use($empleado) {
                            $query->where('tipo_identificacion', $empleado->tipo_identificacion)
                                ->where('identificacion', $empleado->identificacion);
                        });
                }),
            ];
            unset($rules['fecha_registro']);
        };

        return $rules;
    }
}
