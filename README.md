# Backend Prueba Cidenet

Proyecto desarrollado en laravel que maneja la lógica del CRUD de los empleados.

## Configuración

Primer configuar el .env, para eso copiar el .env.example y renombrarlo

```
cp .env.example .env
```

Posteriormente se configuran las variables de entorno de la base de datos (La base de datos debe de estar creada con anterioridad y los datos del usuario deben ser veraces)

``` .env
DB_PORT
DB_DATABASE
DB_USERNAME
DB_PASSWORD
```

Ejecutar los siguientes comandos:

```
composer update
php artisan migrate --seed
php artisan serve --host=0.0.0.0 --port=80
```

Por favor ejecutar el comando tal cual para que funcione en conjunto con el proyecto Font.

Se puede probar si la aplicacion corre exitosamente [aqui](http://localhost/api/empleados)