<?php

namespace Database\Seeders;

use App\Models\Area;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            'Administración',
            'Financiera',
            'Compras',
            'Infraestructura',
            'Operación',
            'Talento Humano',
            'Servicios Varios',
        ];

        foreach($areas as $area) {
            Area::insert([
                'nombre' => $area,
            ]);
        }
    }
}
