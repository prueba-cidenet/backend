<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Enums\{Pais, TipoIdentificacion};
use App\Models\Area;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Empleado>
 */
class EmpleadoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $pais = fake()->randomKey(Pais::cases());

        $primerNombre = strtoupper(fake()->firstName());
        $primerApellido = strtoupper(fake()->unique()->lastName());

        return [
            'primer_nombre' => $primerNombre,
            'otros_nombre' => strtoupper(fake()->firstName()),
            'primer_apellido' => $primerApellido,
            'segundo_apellido' => strtoupper(fake()->lastName()),
            'pais' => Pais::cases()[$pais]->value,
            'tipo_identificacion' => fake()->randomElement(TipoIdentificacion::columnns()),
            'identificacion' => fake()->unique()->regexify('[A-Za-z][0-9]{9}'),
            'correo_electronico' => "{$primerNombre}.{$primerApellido}@cidenet.com.".Pais::cases()[$pais]->dominio(),
            'fecha_ingreso' => now(),
            'fecha_registro' => now(),
            'area_id' => Area::inRandomOrder()
                ->limit(1)
                ->value('id'),
        ];
    }
}
