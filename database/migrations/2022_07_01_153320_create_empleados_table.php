<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\{Estado, Pais, TipoIdentificacion};

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->string('primer_nombre', 20);
            $table->string('otros_nombre', 50)->default('');
            $table->string('primer_apellido', 20);
            $table->string('segundo_apellido', 20);
            $table->enum('pais', Pais::columnns());
            $table->enum('tipo_identificacion', TipoIdentificacion::columnns());
            $table->string('identificacion', 20);
            $table->string('correo_electronico', 300);
            $table->date('fecha_ingreso');
            $table->unsignedBigInteger('area_id');
            $table->enum('estado', Estado::columnns())
                ->default(Estado::Activo->value);
            $table->dateTime('fecha_registro');

            $table->timestamps();

            $table->unique('correo_electronico');
            $table->unique(['tipo_identificacion', 'identificacion']);

            $table->foreign('area_id')->references('id')->on('areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
};
